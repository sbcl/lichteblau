;;; -*- indent-tabs-mode: nil -*-

(defpackage :scratch
  (:use :cl))

(in-package :scratch)

(defclass super () ((a :initarg :a :accessor a)))
(defclass sub (super) ((b :initarg :b :accessor b)))

(defmethod print-object ((object super) stream)
  (print-unreadable-object (object stream :type t :identity t)
    (format stream "a=~A" (gf object))))

(defparameter *i* (make-instance 'sub :a 1 :b 2))

(defmethod gf ((object super))
  (a object))

(defmethod gf ((object sub))
  (b object))

(defun fn (a)
  (gf a))
