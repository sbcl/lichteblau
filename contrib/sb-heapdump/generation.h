/* -*- indent-tabs-mode: nil -*- */
/* this isn't in gencgc-internal.h, so we need to copy&paste it */

enum {
    HIGHEST_NORMAL_GENERATION = 5,
    PSEUDO_STATIC_GENERATION,
    SCRATCH_GENERATION,
    NUM_GENERATIONS
};

struct generation {

    /* the first page that gc_alloc() checks on its next call */
    page_index_t alloc_start_page;

    /* the first page that gc_alloc_unboxed() checks on its next call */
    page_index_t alloc_unboxed_start_page;

    /* the first page that gc_alloc_large (boxed) considers on its next
     * call. (Although it always allocates after the boxed_region.) */
    page_index_t alloc_large_start_page;

    /* the first page that gc_alloc_large (unboxed) considers on its
     * next call. (Although it always allocates after the
     * current_unboxed_region.) */
    page_index_t alloc_large_unboxed_start_page;

    /* the bytes allocated to this generation */
    long bytes_allocated;

    /* the number of bytes at which to trigger a GC */
    long gc_trigger;

    /* to calculate a new level for gc_trigger */
    long bytes_consed_between_gc;

    /* the number of GCs since the last raise */
    int num_gc;

    /* the average age after which a GC will raise objects to the
     * next generation */
    int trigger_age;

    /* the cumulative sum of the bytes allocated to this generation. It is
     * cleared after a GC on this generations, and update before new
     * objects are added from a GC of a younger generation. Dividing by
     * the bytes_allocated will give the average age of the memory in
     * this generation since its last GC. */
    long cum_sum_bytes_allocated;

    /* a minimum average memory age before a GC will occur helps
     * prevent a GC when a large number of new live objects have been
     * added, in which case a GC could be a waste of time */
    double min_av_mem_age;
};
