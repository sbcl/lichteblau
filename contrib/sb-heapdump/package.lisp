;;; -*- indent-tabs-mode: nil -*-

(in-package :cl-user)

(defpackage :sb-heapdump
  (:use :cl)
  (:shadow #:defun #:lambda)
  (:export #:*dumpload-verbose* #:*dump-verbose* #:*central-registry*
           #:dump-object #:dump-packages #:dump-system
           #:load-dumpfile
           #:relocate-dumpfile #:relocate-dumpfiles
           #:make-executable))

(in-package :sb-heapdump)

;; Give lambdas a name, since SBCL prints them only as ((LAMBDA ())) in
;; backtraces otherwise, and that's not good enough with the large number
;; of functions we have that use the lambda trick.
(defmacro defun (name (&rest args) &body body)
  (let ((declarationp (and (listp (car body)) (eq (caar body) 'declare))))
    `(cl:defun ,name ,args
       ,@(when declarationp
           (list (car body)))
       (macrolet ((lambda ((&rest args) &body body)
                    `(sb-int:named-lambda ,'(lambda ,name) ,args ,@body)))
         ,@(if declarationp (cdr body) body)))))

(defmacro lambda ((&rest args) &body body)
  `(cl:lambda ,args ,@body))
