/* -*- indent-tabs-mode: nil -*- */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include <sys/stat.h>
#include <fcntl.h>

static void
syserr(char *str)
{
        perror(str);
        exit(1);
}

#define FORMAT_CONTROL "(sb-heapdump:load-dumpfile \"%s\" :start %ld :end %ld)"
static char *
format_form(char *this, long start, long end)
{
        int ndigits = (int) (log(ULONG_MAX) / log(10)) + 1;
        int n = strlen(FORMAT_CONTROL) + 2 * ndigits;
        char *form = malloc(n + 1);
        if (!form) exit(1);
        snprintf(form, n, FORMAT_CONTROL, this, start, end);
        return form;
}

static char *extra_args[] = {
        "sbcl",
        "--noinform",
        "--userinit", "/dev/null",
        "--eval",
        "(unless (find-package :sb-heapdump)"
        "  (format t \"~&error: core file does not include sb-heapdump~%\")"
        "  (sb-ext:quit :unix-status 1))",
        "--eval", 0,
        "--eval", "(sb-ext:quit :unix-status 0)",
        "--end-toplevel-options",
        0
};

static void
parse_file(char *this, long *start, long *end)
{
        int fd = open(this, O_RDONLY, 0);
        if (fd == -1) syserr("open");
        if ( (*end = lseek(fd, -sizeof(long), SEEK_END)) == -1)
                syserr("lseek");
        if (read(fd, start, sizeof(long)) != sizeof(long)) syserr("read");
        close(fd);
}

int
main(int argc, char **argv)
{
        int n = sizeof(extra_args) / sizeof(char *) - 1;
        char *this = argv[0];
        char **args = malloc((n + argc + 1) * sizeof(char *));
        int i;
        long start, end;

        if (!args) syserr("malloc");
        if (strchr(this, '"') || strchr(this, '\\')) {
                fputs("error: file name contains invalid character\n", stderr);
                exit(1);
        }
        parse_file(this, &start, &end);

        for (i = 0; i < n; i++)
                if (extra_args[i])
                        args[i] = extra_args[i];
                else
                        args[i] = format_form(this, start, end);
        for (i = 1; i < argc; i++)
                args[n + i] = argv[i];
        args[n + argc + 1] = 0;

        execvp("sbcl", args);
        perror("exec");
        fputs("error: cannot find SBCL runtime environment\n", stderr);
        fputs("make sure sbcl(1) can be found in $PATH\n", stderr);
        exit(1);
}
