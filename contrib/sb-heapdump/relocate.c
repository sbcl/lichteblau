/* -*- indent-tabs-mode: nil -*- */

/* Copyright (c) 2006 David Lichteblau
 * partly derived from SBCL source code (gc-common.c/gencgc.c)
 *
 * Tested on x86, x86-64, and PPC.
 *
 * When using this code to relocate memory not dumped by sb-heapdump,
 * read the note in relocate_simple_vector.
 */
/*
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include "genesis/config.h"
#include "validate.h"
#include "gc.h"
#ifdef LISP_FEATURE_GENCGC
#include "gencgc-internal.h"
#else
#include "cheneygc-internal.h"
#endif
#include "gc-internal.h"
#include "generation.h"
#include "runtime.h"
#include "interr.h"
#include "genesis/fdefn.h"
#include "genesis/closure.h"
#include "genesis/instance.h"
#include "genesis/layout.h"
#include "genesis/code.h"
#include "genesis/simple-fun.h"
#include "genesis/vector.h"

/*
 * stuff from src/runtime not declared in the official headers
 */
#ifdef LISP_FEATURE_GENCGC
extern unsigned long bytes_allocated;
extern struct generation generations[NUM_GENERATIONS];
extern long large_object_size;
page_index_t gc_find_freeish_pages(long *, long, int);
#endif

/*
 * our stuff
 */
#define ALIGN(len) CEILING(len, 2)
#define RELOCATE_BOXED 0
#define RELOCATE_IMMEDIATE 0

#ifndef LISP_FEATURE_GENCGC
#define PAGE_BYTES 0x1000
#endif

struct relocator {
        long *start;
        long *end;
        long displacement;
        void *baseptr;
};

typedef long (*relocfn)(long *, struct relocator *);
static relocfn reloctab[256];

static int reloctab_initialized = 0;

static void relocate_init();
static void relocate(long *, long nwords, long *old_start, long displacement);
static void sub_relocate(long *ptr, long nwords, struct relocator *ctx);


/*
 * heap file mapping
 */
#ifdef LISP_FEATURE_GENCGC
static void
find_free_pages(long *start_page, long *end_page, long nbytes)
{
        long los = large_object_size;

        large_object_size = 0;
        *end_page = 1 + gc_find_freeish_pages(start_page, nbytes, 0);
        large_object_size = los;
}

/* FIXME: This used to be 2, which stopped working in SBCL 0.9.13.22
 * (Gen. 6 also works, but 0 is arguable better than that.) */
#define GEN 0

void *
map_dumpfile(int fd, long offset, int verbose)
{
        unsigned long length;
        void *base = 0;
        void *old_base;
        long start_page, end_page;
        long npages;
        long i;

        if (!reloctab_initialized) {
                relocate_init();
                reloctab_initialized = 1;
        }

        if (lseek(fd, offset, SEEK_SET) == -1) {
                perror("lseek");
                lose("map_dumpfile: cannot seek to segment");
        }
        if (read(fd, &old_base, sizeof(long)) != sizeof(long)
            || read(fd, &length, sizeof(long)) != sizeof(long))
        {
                perror("read");
                lose("map_dumpfile: cannot read header");
        }
        npages = (length + PAGE_BYTES - 1) / PAGE_BYTES;

        if ( (start_page = find_page_index(old_base)) != -1) {
                end_page = start_page + npages;
                for (i = start_page; i < end_page; i++)
                        if (page_table[i].allocated != FREE_PAGE_FLAG)
                                break;
                if (i == end_page)
                        base = old_base;
        }
        if (!base) {
                find_free_pages(&start_page, &end_page, length);
                base = page_address(start_page);
                if (verbose) {
                        printf("\n; relocating heap file from 0x%08lx"
                               " to 0x%08lx\n",
                               (long) old_base,
                               (long) base);
                        fflush(stdout);
                }
        }

        if (base != mmap(base,
                         length,
                         PROT_READ | PROT_WRITE,
                         MAP_FIXED | MAP_PRIVATE,
                         fd,
                         offset))
        {
                perror("mmap");
                lose("map_dumpfile: cannot mmap heap file");
        }
        if (base != old_base)
                relocate(base, length/N_WORD_BYTES, old_base, base-old_base);

        os_protect(base,
                   npages * PAGE_BYTES,
#ifdef WRITE_PROTECT
                   OS_VM_PROT_READ | OS_VM_PROT_EXECUTE
#else
                   OS_VM_PROT_ALL | OS_VM_PROT_EXECUTE
#endif
                );

        for (i = 0; i < npages; i++) {
                long page = start_page + i;
                page_table[page].allocated = BOXED_PAGE_FLAG;
                page_table[page].gen = GEN;
                page_table[page].large_object = 0;
                page_table[page].first_object_offset = -(PAGE_BYTES * i);
                page_table[page].bytes_used = PAGE_BYTES;
#ifdef WRITE_PROTECT
                page_table[page].write_protected = 1;
#else
                page_table[page].write_protected = 0;
#endif
                page_table[page].write_protected_cleared = 0;
                page_table[page].dont_move = 0;
        }
        page_table[end_page - 1].bytes_used = length - PAGE_BYTES * (npages-1);
        generations[GEN].bytes_allocated += length;
#if 0
        /* fixme: do we need these? */
        bytes_allocated += length;
        generations[GEN].cum_sum_bytes_allocated += length;
#endif

        if (last_free_page < end_page)
                last_free_page = end_page;
        SetSymbolValue(ALLOCATION_POINTER,
                       (lispobj)(((char *)DYNAMIC_SPACE_START)
                                 + last_free_page*PAGE_BYTES),
                       0);

        return base;
}
#else
void *
map_dumpfile(int fd, long offset, int verbose)
{
        unsigned long length;
        void *base;
        void *old_base;

        if (!reloctab_initialized) {
                relocate_init();
                reloctab_initialized = 1;
        }

        if (lseek(fd, offset, SEEK_SET) == -1) {
                perror("lseek");
                lose("map_dumpfile: cannot seek to segment");
        }
        if (read(fd, &old_base, sizeof(long)) != sizeof(long)
            || read(fd, &length, sizeof(long)) != sizeof(long))
        {
                perror("read");
                lose("map_dumpfile: cannot read header");
        }

        base = (void *) CEILING((long)dynamic_space_free_pointer, PAGE_BYTES);
        dynamic_space_free_pointer = base + length;

        if (base != mmap(base,
                         length,
                         PROT_READ | PROT_WRITE,
                         MAP_FIXED | MAP_PRIVATE,
                         fd,
                         offset))
        {
                perror("mmap");
                lose("map_dumpfile: cannot mmap heap file");
        }
        if (verbose) {
                printf("\n; relocating heap file from 0x%08lx to 0x%08lx\n",
                       (long) old_base,
                       (long) base);
                fflush(stdout);
        }
        relocate(base, length/N_WORD_BYTES, old_base, base-old_base);

        os_flush_icache((os_vm_address_t) base, length);

        return base;
}
#endif

long
relocate_dumpfile(int fd, long offset, long *new_base)
{
        long length;
        void *tmp;
        long *old_base;
        long displacement;

        if (!reloctab_initialized) {
                relocate_init();
                reloctab_initialized = 1;
        }

        if (lseek(fd, offset, SEEK_SET) == -1) {
                perror("lseek");
                lose("map_dumpfile: cannot seek to segment");
        }
        if (read(fd, &old_base, sizeof(long)) != sizeof(long)
            || read(fd, &length, sizeof(long)) != sizeof(long))
        {
                perror("read");
                lose("relocate_dumpfile: cannot read header");
        }

        tmp = mmap(0, length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, offset);
        if (tmp == MAP_FAILED) {
                perror("mmap");
                lose("relocate_dumpfile: cannot map heap file");
        }
#ifdef LISP_FEATURE_GENCGC
        if ((long) tmp % PAGE_BYTES != 0)
                lose("relocate_dumpfile: bad base address");
#endif

        displacement = (void *) new_base - (void *) old_base;
        relocate(tmp, length/N_WORD_BYTES, old_base, displacement);
        *((long **) tmp) = new_base;

        if (munmap(tmp, length) == -1) {
                perror("munmap");
                lose("relocate_dumpfile: cannot unmap heap file");
        }
        return length;
}


/*
 * relocation
 */
static void *
natify(lispobj thing, struct relocator *ctx)
{
        /* Same as `native_pointer' if tempspace == newspace.  Else,
         * turn the result into a tempspace pointer.
         * This is for relocate_dumpfile. */
        void *old_start = (void *) ctx->start;
        void *new_start = old_start + ctx->displacement;
        void *ptr = native_pointer((long) thing);
        long offset = ptr - new_start;
        return (void *) ctx->baseptr + offset;
}

#ifdef LISP_FEATURE_X86
static void *
oldify(void *ptr, struct relocator *ctx)
{
        return (void *) ctx->start + (ptr - (void *) ctx->baseptr);
}
#endif

static void
relocate(long *ptr, long nwords, long *old_start, long displacement)
{
        struct relocator ctx;

        ctx.baseptr = ptr;
        ctx.start = old_start;
        ctx.end = old_start + nwords;
        ctx.displacement = displacement;

        sub_relocate(ptr, nwords, &ctx);
}

static void
sub_relocate(long *ptr, long nwords, struct relocator *ctx)
{
        long *p;
        long *q = ptr + nwords;
        long nrelocated;

        for (p = ptr; p < q; p += nrelocated) {
                long word = *p;
                if (is_lisp_pointer(word)) {
                        long *address = (long *) native_pointer(word);
                        if (ctx->start <= address && address < ctx->end)
                                *p += ctx->displacement;
                        nrelocated = 1;
                } else {
                        relocfn fn = reloctab[widetag_of(word)];
                        if (fn)
                                nrelocated = fn(p, ctx);
                        else
                                nrelocated = 1;
                }
        }
}

static long
relocate_lose(long *ptr, struct relocator *ctx)
{
        lose("no relocation function for header 0x%08x at 0x%08x\n",
             *ptr, ptr);
        return 0;
}

static long
relocate_unboxed(long *ptr, struct relocator *ctx)
{
        return ALIGN(HeaderValue(*ptr) + 1);
}

static long
relocate_raw_vector(long *ptr, struct relocator *ctx)
{
        return sizetab[widetag_of(*ptr)]((void *) ptr);
}

static long
relocate_simple_vector(long *ptr, struct relocator *ctx)
{
        /* note: we leave the simple vector header as-is, assuming that
         * the dumper has marked hash tables needing a re-hash already.
         * If using the relocation routine is to be used for pages not
         * written by sb-heapdump, at least replace
         * vector-valid-hashing-subtype with
         * sb-vm:vector-must-rehash-subtype here. */
        return 2;
}

static long
relocate_fdefn(long *ptr, struct relocator *ctx)
{
        struct fdefn *fdefn = (struct fdefn *) ptr;
        char *nontramp_raw_addr = (char *) fdefn->fun + FUN_RAW_ADDR_OFFSET;

        sub_relocate(ptr + 1, 2, ctx);
        if (fdefn->raw_addr == nontramp_raw_addr)
                fdefn->raw_addr = (char *)(fdefn->fun + FUN_RAW_ADDR_OFFSET);
        return sizeof(struct fdefn) / sizeof(lispobj);
}

#if defined(LISP_FEATURE_X86) || defined(LISP_FEATURE_X86_64)
static long
relocate_closure_header(long *ptr, struct relocator *ctx)
{
        struct closure *closure = (struct closure *) ptr;
        long fun = (long) closure->fun - FUN_RAW_ADDR_OFFSET;
        sub_relocate(&fun, 1, ctx);
        closure->fun = fun + FUN_RAW_ADDR_OFFSET;
        return 2;
}
#endif

static long
relocate_instance(long *ptr, struct relocator *ctx)
{
        lispobj nuntagged;
        struct instance *instance = (struct instance *) ptr;
        long ntotal = HeaderValue(*ptr);

        sub_relocate((long *) &instance->slots[0], 1, ctx);
        if (fixnump(instance->slots[0]))
                /* If the layout is a fixup, the dumper stores `nuntagged'
                 * here for us to find. */
                nuntagged = instance->slots[0];
        else {
                struct layout *layout = natify(instance->slots[0], ctx);
                nuntagged = layout->n_untagged_slots;
        }

        sub_relocate(ptr + 2, ntotal - fixnum_value(nuntagged) - 1, ctx);
        return ntotal + 1;
}

static long
relocate_code_header(long *ptr, struct relocator *ctx)
{
        long header = *ptr;
        struct code *code = (struct code *) ptr;
        long n_header_words = HeaderValue(header);
        long n_code_words = fixnum_value(code->code_size);
        long n_words = ALIGN(n_header_words + n_code_words);
        lispobj ep;

        sub_relocate(ptr + 1, n_header_words - 1, ctx);

        ep = code->entry_points;
        while (ep != NIL) {
                struct simple_fun *fun = natify(ep, ctx);
#if defined(LISP_FEATURE_X86) || defined(LISP_FEATURE_X86_64)
                fun->self = (long) ep + FUN_RAW_ADDR_OFFSET;
#else
                fun->self = ep;
#endif
                sub_relocate((void *) &fun->next, 1, ctx);
                sub_relocate((void *) &fun->name, 1, ctx);
                sub_relocate((void *) &fun->arglist, 1, ctx);
                sub_relocate((void *) &fun->type, 1, ctx);
                sub_relocate((void *) &fun->xrefs, 1, ctx);
                ep = fun->next;
        }

#ifdef LISP_FEATURE_X86
        if (is_lisp_pointer(code->constants[0])) {
                long word_displacement = ctx->displacement / N_WORD_BYTES;
                char *code_start
                        = ((char *) code) + n_header_words * N_WORD_BYTES;
                long *old_start = oldify(ptr, ctx);
                long *old_end = old_start + n_words;

                struct vector *fixups = natify(code->constants[0], ctx);
                long n = fixnum_value(fixups->length);
                long i;

                for (i = 0; i < n; i++) {
                        unsigned long offset = fixups->data[i];
                        long **place = (long **) (code_start + offset);
                        long *old_value = *place;

                        if (old_start <= old_value && old_value < old_end)
                                *place = old_value + word_displacement;
                        else
                                *place = old_value - word_displacement;
                }
        }
#endif

        return n_words;
}

void
relocate_init()
{
        int i;

        for (i = 0; i < ((sizeof reloctab)/(sizeof reloctab[0])); i++)
                reloctab[i] = relocate_lose;

        for (i = 0; i < (1<<(N_WIDETAG_BITS-N_LOWTAG_BITS)); i++) {
                reloctab[EVEN_FIXNUM_LOWTAG|(i<<N_LOWTAG_BITS)]
                        = RELOCATE_IMMEDIATE;
                reloctab[ODD_FIXNUM_LOWTAG|(i<<N_LOWTAG_BITS)]
                        = RELOCATE_IMMEDIATE;
        }

        reloctab[BIGNUM_WIDETAG] = relocate_unboxed;
        reloctab[RATIO_WIDETAG] = RELOCATE_BOXED;
#if N_WORD_BITS == 64
        reloctab[SINGLE_FLOAT_WIDETAG] = RELOCATE_IMMEDIATE;
#else
        reloctab[SINGLE_FLOAT_WIDETAG] = relocate_unboxed;
#endif
        reloctab[DOUBLE_FLOAT_WIDETAG] = relocate_unboxed;
#ifdef LONG_FLOAT_WIDETAG
        reloctab[LONG_FLOAT_WIDETAG] = relocate_unboxed;
#endif
        reloctab[COMPLEX_WIDETAG] = RELOCATE_BOXED;
#ifdef COMPLEX_SINGLE_FLOAT_WIDETAG
        reloctab[COMPLEX_SINGLE_FLOAT_WIDETAG] = relocate_unboxed;
#endif
#ifdef COMPLEX_DOUBLE_FLOAT_WIDETAG
        reloctab[COMPLEX_DOUBLE_FLOAT_WIDETAG] = relocate_unboxed;
#endif
#ifdef COMPLEX_LONG_FLOAT_WIDETAG
        reloctab[COMPLEX_LONG_FLOAT_WIDETAG] = relocate_unboxed;
#endif
        reloctab[SIMPLE_ARRAY_WIDETAG] = RELOCATE_BOXED;
        reloctab[SIMPLE_BASE_STRING_WIDETAG] = relocate_raw_vector;
#ifdef SIMPLE_CHARACTER_STRING_WIDETAG
        reloctab[SIMPLE_CHARACTER_STRING_WIDETAG] = relocate_raw_vector;
#endif
        reloctab[SIMPLE_BIT_VECTOR_WIDETAG] = relocate_raw_vector;
        reloctab[SIMPLE_VECTOR_WIDETAG] = relocate_simple_vector;
        reloctab[SIMPLE_ARRAY_NIL_WIDETAG] = relocate_raw_vector;
        reloctab[SIMPLE_ARRAY_UNSIGNED_BYTE_2_WIDETAG] = relocate_raw_vector;
        reloctab[SIMPLE_ARRAY_UNSIGNED_BYTE_4_WIDETAG] = relocate_raw_vector;
        reloctab[SIMPLE_ARRAY_UNSIGNED_BYTE_7_WIDETAG] = relocate_raw_vector;
        reloctab[SIMPLE_ARRAY_UNSIGNED_BYTE_8_WIDETAG] = relocate_raw_vector;
        reloctab[SIMPLE_ARRAY_UNSIGNED_BYTE_15_WIDETAG] = relocate_raw_vector;
        reloctab[SIMPLE_ARRAY_UNSIGNED_BYTE_16_WIDETAG] = relocate_raw_vector;
#ifdef SIMPLE_ARRAY_UNSIGNED_BYTE_29_WIDETAG
        reloctab[SIMPLE_ARRAY_UNSIGNED_BYTE_29_WIDETAG] = relocate_raw_vector;
#endif
        reloctab[SIMPLE_ARRAY_UNSIGNED_BYTE_31_WIDETAG] = relocate_raw_vector;
        reloctab[SIMPLE_ARRAY_UNSIGNED_BYTE_32_WIDETAG] = relocate_raw_vector;
#ifdef SIMPLE_ARRAY_UNSIGNED_BYTE_60_WIDETAG
        reloctab[SIMPLE_ARRAY_UNSIGNED_BYTE_60_WIDETAG] = relocate_raw_vector;
#endif
#ifdef SIMPLE_ARRAY_UNSIGNED_BYTE_63_WIDETAG
        reloctab[SIMPLE_ARRAY_UNSIGNED_BYTE_63_WIDETAG] = relocate_raw_vector;
#endif
#ifdef SIMPLE_ARRAY_UNSIGNED_BYTE_64_WIDETAG
        reloctab[SIMPLE_ARRAY_UNSIGNED_BYTE_64_WIDETAG] = relocate_raw_vector;
#endif
#ifdef SIMPLE_ARRAY_SIGNED_BYTE_8_WIDETAG
        reloctab[SIMPLE_ARRAY_SIGNED_BYTE_8_WIDETAG] = relocate_raw_vector;
#endif
#ifdef SIMPLE_ARRAY_SIGNED_BYTE_16_WIDETAG
        reloctab[SIMPLE_ARRAY_SIGNED_BYTE_16_WIDETAG] = relocate_raw_vector;
#endif
#ifdef SIMPLE_ARRAY_SIGNED_BYTE_30_WIDETAG
        reloctab[SIMPLE_ARRAY_SIGNED_BYTE_30_WIDETAG] = relocate_raw_vector;
#endif
#ifdef SIMPLE_ARRAY_SIGNED_BYTE_32_WIDETAG
        reloctab[SIMPLE_ARRAY_SIGNED_BYTE_32_WIDETAG] = relocate_raw_vector;
#endif
#ifdef SIMPLE_ARRAY_SIGNED_BYTE_61_WIDETAG
        reloctab[SIMPLE_ARRAY_SIGNED_BYTE_61_WIDETAG] = relocate_raw_vector;
#endif
#ifdef SIMPLE_ARRAY_SIGNED_BYTE_64_WIDETAG
        reloctab[SIMPLE_ARRAY_SIGNED_BYTE_64_WIDETAG] = relocate_raw_vector;
#endif
        reloctab[SIMPLE_ARRAY_SINGLE_FLOAT_WIDETAG] = relocate_raw_vector;
        reloctab[SIMPLE_ARRAY_DOUBLE_FLOAT_WIDETAG] = relocate_raw_vector;
#ifdef SIMPLE_ARRAY_LONG_FLOAT_WIDETAG
        reloctab[SIMPLE_ARRAY_LONG_FLOAT_WIDETAG] = relocate_raw_vector;
#endif
#ifdef SIMPLE_ARRAY_COMPLEX_SINGLE_FLOAT_WIDETAG
        reloctab[SIMPLE_ARRAY_COMPLEX_SINGLE_FLOAT_WIDETAG]
                = relocate_raw_vector;
#endif
#ifdef SIMPLE_ARRAY_COMPLEX_DOUBLE_FLOAT_WIDETAG
        reloctab[SIMPLE_ARRAY_COMPLEX_DOUBLE_FLOAT_WIDETAG]
                = relocate_raw_vector;
#endif
#ifdef SIMPLE_ARRAY_COMPLEX_LONG_FLOAT_WIDETAG
        reloctab[SIMPLE_ARRAY_COMPLEX_LONG_FLOAT_WIDETAG]
                = relocate_raw_vector;
#endif
        reloctab[COMPLEX_BASE_STRING_WIDETAG] = RELOCATE_BOXED;
#ifdef COMPLEX_CHARACTER_STRING_WIDETAG
        reloctab[COMPLEX_CHARACTER_STRING_WIDETAG] = RELOCATE_BOXED;
#endif
        reloctab[COMPLEX_VECTOR_NIL_WIDETAG] = RELOCATE_BOXED;
        reloctab[COMPLEX_BIT_VECTOR_WIDETAG] = RELOCATE_BOXED;
        reloctab[COMPLEX_VECTOR_WIDETAG] = RELOCATE_BOXED;
        reloctab[COMPLEX_ARRAY_WIDETAG] = RELOCATE_BOXED;
        reloctab[CODE_HEADER_WIDETAG] = relocate_code_header;
#ifndef LISP_FEATURE_GENCGC     /* FIXME ..._X86 ? */
        reloctab[SIMPLE_FUN_HEADER_WIDETAG] = relocate_lose;
        reloctab[RETURN_PC_HEADER_WIDETAG] = relocate_lose;
#endif
        reloctab[FUNCALLABLE_INSTANCE_HEADER_WIDETAG] = RELOCATE_BOXED;
#if defined(LISP_FEATURE_X86) || defined(LISP_FEATURE_X86_64)
        reloctab[CLOSURE_HEADER_WIDETAG] = relocate_closure_header;
#else
        reloctab[CLOSURE_HEADER_WIDETAG] = RELOCATE_BOXED;
#endif
        reloctab[VALUE_CELL_HEADER_WIDETAG] = RELOCATE_BOXED;
        reloctab[SYMBOL_HEADER_WIDETAG] = RELOCATE_BOXED;
        reloctab[CHARACTER_WIDETAG] = RELOCATE_IMMEDIATE;
        reloctab[SAP_WIDETAG] = relocate_unboxed;
        reloctab[UNBOUND_MARKER_WIDETAG] = RELOCATE_IMMEDIATE;
        reloctab[NO_TLS_VALUE_MARKER_WIDETAG] = RELOCATE_IMMEDIATE;
        reloctab[WEAK_POINTER_WIDETAG] = RELOCATE_BOXED;
        reloctab[INSTANCE_HEADER_WIDETAG] = relocate_instance;
#ifdef LISP_FEATURE_SPARC
        reloctab[FDEFN_WIDETAG] = RELOCATE_BOXED;
#else
        reloctab[FDEFN_WIDETAG] = relocate_fdefn;
#endif
}
