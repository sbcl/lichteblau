(defpackage :sb-heapdump-system		;-*- mode: lisp -*-
  (:use :asdf :cl))
(in-package :sb-heapdump-system)

(defsystem sb-heapdump
    #+sb-building-contrib :pathname
    #+sb-building-contrib "SYS:CONTRIB;SB-HEAPDUMP;"
    :serial t
    :components ((:file "package")
		 (:file "common")
		 (:file "patch")
		 (:file "dump")
		 (:file "load")
		 (:file "pack")
		 (:file "module"))
    :depends-on ())

;; fixme
(sb-alien:load-shared-object
 (make-pathname
  :name "relocate"
  :type "so"
  :version nil
  :defaults (component-relative-pathname (find-system :sb-heapdump))))

(defmethod perform :after ((o load-op) (c (eql (find-system 'sb-heapdump))))
  (provide 'sb-heapdump))

(defmethod perform ((o test-op) (c (eql (find-system 'sb-heapdump))))
  (oos 'load-op 'sb-heapdump-tests)
  (oos 'test-op 'sb-heapdump-tests))

(defsystem sb-heapdump-tests
    :depends-on (sb-rt)
    :components ((:file "testpack")
		 (:file "test")))

(defmethod perform ((o test-op) (c (eql (find-system :sb-heapdump-tests))))
  (or (funcall (find-symbol "DO-TESTS" "SB-RT"))
      (error "test-op failed")))
