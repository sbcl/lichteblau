;;; -*- indent-tabs-mode: nil -*-
;;;
;;; Copyright (c) 2006 David Lichteblau
;;;
;;; Permission is hereby granted, free of charge, to any person
;;; obtaining a copy of this software and associated documentation files
;;; (the "Software"), to deal in the Software without restriction,
;;; including without limitation the rights to use, copy, modify, merge,
;;; publish, distribute, sublicense, and/or sell copies of the Software,
;;; and to permit persons to whom the Software is furnished to do so,
;;; subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be
;;; included in all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
;;; BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
;;; ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
;;; CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;;; SOFTWARE.

(in-package :sb-heapdump)

(defconstant +n+ sb-vm:n-word-bytes)
(defconstant +2n+ (* 2 +n+))

(defstruct (header (:type vector))
  object
  fixups
  initializer
  customizer)

(macrolet ((doit (&rest names)
             `(progn
                (defvar *fixup-names* ,(coerce names 'vector))
                ,@(loop
                      for name in names
                      for i from 0
                      collect `(defconstant ,name ,i)))))
  ;; order matters
  (doit +package-fixup+
        +symbol-fixup+
        +classoid-fixup+
        +layout-fixup+
        +fdefn-fixup+
        +named-type-fixup+
        +array-type-fixup+
        +class-fixup+
        +function-fixup+
        +ctor-fixup+
        +slot-accessor-fixup+
        #+#.(cl:if (cl:find-symbol "METHOD-FUNCTION-PLIST" :sb-pcl)
                '(and)
                '(or))
        +fast-method-fixup+
        +raw-address-fixup+
        +variable-fixup+
        +foreign-fixup+
        +user-fixup+))

(defstruct (fixup
            (:type vector)
            (:constructor make-fixup (type id))
            (:constructor make-symbol-fixup (type id2 id))
            (:constructor make-fast-method-fixup (type id id2))
            (:constructor make-foreign-fixup (type id id2))
            (:constructor make-user-fixup (type id id2)))
  type
  id
  id2
  locations)
