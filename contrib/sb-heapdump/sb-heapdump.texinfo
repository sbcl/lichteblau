@node sb-heapdump
@section sb-heapdump

sb-heapdump is a library for SBCL which writes graphs of Lisp objects to
disk in the same format SBCL normally uses in memory.

sb-heapdump is currently supported on the x86, x86-64, and PPC
platforms.

@menu
* Reading heapfiles::
* Dumping objects and packages::
* Optimizing heapfiles::
* Integration into REQUIRE and ASDF::
* Executable files::
* DUMP-OBJECT behaviour for specific classes::
* DUMP-PACKAGES details::
@end menu

Features:
@itemize
@item
  sb-heapdump supports @emph{all} kinds of Lisp objects SBCL knows
@item
  reads data back very quickly
@item
  is highly SBCL specific.  No attempt at portability is made.
@item
  does expressly @emph{not} define a forward- or backward-compatible format.
    Upgrades to SBCL @emph{will} break support for previously dumped heapfiles.
@item
  as an extension, can dump entire packages with all their definitions
@end itemize

FIXME:

@itemize
@item
    See below for various FIXME comments.
@item
    sb-heapdump keeps a global hash table of all code objects
    referencing foreign symbols.  Make sure to require sb-heapdump
    @emph{before} loading any fasls referencing the linkage table.
@item
    Also note that an effort is made to fully support generic functions
    and CLOS classes and instances, but support for this is considered
    experimental until someone tells me that all the various caches CLOS
    keeps are faithfully preserved by dumping.
@item
    separating tagged and untagged objects would help gc performance
@item
    so would starting a new region every few pages (I think)
@item
    what about functions without a fixup vector?
@end itemize

@node Reading heapfiles
@subsection Reading heapfiles

@deffn {Function} LOAD-DUMPFILE (pathname &key customizer suppress-initializer)
  Load the dumpfile from PATHNAME, then run the initializer
  specified included in the dumpfile, if any.  Call the initializer with
  the object that has been loaded back and return the initializer's
  return value.  If no initializer is run, return the object directly.

  Multiple heap file segments can be concatenated into one file.  In
  this case, LOAD-DUMPFILE will load all segments found in orde.  The
  last segment's value will be returned.  Concatenation can be done
  using :if-exists :append while dumping, or simply using cat(1).

  Keyword arguments:
@itemize
  @item
    SUPRRESS-INITIALIZER (default nil) -- if true, suppress running the
      initializer and directly return the object in the dump file.
  @item
    CUSTOMIZER -- override the LOAD-TIME-CUSTOMIZER specified when dumping.
      See below.
@end itemize
@end deffn

@node Dumping objects and packages
@subsection Dumping objects and packages

@deffn {Function} DUMP-OBJECT (object pathname &key if-exists initializer customizer load-time-customizer force print-statistics base-address)
  Write OBJECT to a heapfile at PATHNAME.

  Recursively walk all the graph of objects referenced from OBJECT and
  dump them too, except for objects assumed to be `unique'.  Unique
  objects are not dumped unless specified using FORCE; instead they will
  be assumed to exist in the target image already and references to them
  will be fixup up at load time.  It is an error if such an object
  cannot be found then.  See below for a details.

  Keyword arguments:
@itemize
@item
    IF-EXISTS (one of :error (default), :rename-and-delete, or :append) --
      passed to OPEN.  When using :append, a new segment fill be added
      to an existing heap file.  See LOAD-DUMPFILE for details.
@item
    INITIALIZER -- if specified, a function object of one argument to be
      run after the heap file has been loaded back into memory by
      LOAD-DUMPFILE. See there for details.
@item
    CUSTOMIZER -- An optional function of one argument called for every
     object dumped.  Possible return values:
@itemize
@item
       (a) T
           Dumping of the object will then proceed normally.)
@item
       (b) As multiple values, (NIL; replacement object)
           The replacement value will be substituted for every reference
           to the original value while dumping.
@item
       (c) As multiple values, (:FIXUP; data1; data2)
           The  object will be replaced by a fixup to be resolved at load time.
	   LOAD-DUMPFILE will call LOAD-TIME-CUSTOMIZER with data1 and data2
	   as its arguments and substitute references to the original object
	   for its return value.
@end itemize
@item
    LOAD-TIME-CUSTOMIZER -- function to be dumped into the heapfile to
      resolve user fixups as specified in the description of CUSTOMIZER.
      Can be overriden at load time using the CUSTOMIZER argument to
      LOAD-HEAPFILE.
@item
    FORCE -- An optional list of objects specifying that these objects
      are to be dumped directly even if they would have been replaced
      with fixups otherwise.
@item
    PRINT-STATISTICS (boolean) -- print statistics about the number and
      kinds of objects dumped before returning
@item
    BASE-ADDRESS -- a memory address as an integer, aligned to a page
      boundary.  Write the heapfile so that it can be mapped without
      relocation if memory starting with BASE-ADDRESS is free (and lies
      within dynamic space).
@end itemize
@end deffn

@deffn {Function} DUMP-PACKAGES (packages pathname &key if-exists print-statistics customizer load-time-customizer initializer base-address systems system-packages)

  Dump the entire PACKAGES specified into a dumpfile.  This is roughly
  equivalent to
    (DUMP-OBJECT packages pathname :FORCE packages)
  except that it collects additional information about objects named by
  symbols in the packages specified (including function and class
  definitions) and makes sure to restore this data after loading.

  Keyword arguments:
@itemize
@item
    INITIALIZER -- called with the list of packages after other
      initialization has been completed.
@item
    IF-EXISTS, PRINT-STATISTICS, CUSTOMIZER, LOAD-TIME-CUSTOMIZER,
      BASE-ADDRESS --
      cf. DUMP-OBJECT
@item
    SYSTEMS -- list of ASDF system designators.  If specified, prepend
      a segment to the dumpfile containing the ASDF systems with an
      initializer that will restore them and require their dependencies
      before loading the main segment containing PACKAGES. 
@item
    SYSTEM-PACKAGES -- list of packages that SYSTEMS were defined in.
@end itemize
@end deffn

Note that Lisp software can cause extensive changes to a Lisp image
while it is loaded and run, many of which are not necessarily reflected
in the actualy home package(s) of the software.  DUMP-PACKAGES cannot
automatically determine which parts of the current Lisp image "belong"
to the software that is to be dumped.  To make such software work with
DUMP-PACKAGES, users will often have to customize the dumping
procedure.  One way to do this is by specifying a custom INITIALIZER.
For example, if the software stores data on the plist of symbols not
contained in the packages to be dumped, write an initializer that
restores these plists after loading.


@node Optimizing heapfiles
@subsection Optimizing heapfiles

Heap files that cannot be mapped to the base-address they were targetted
for will be relocated automatically.  Multiple heap files expected to be
loaded together (and heap files containing several segments) can be
relocated in advance to avoid overlap and unnecessary relocation at load
time.

(However, note that relocation is relatively fast and heap files
generated by DUMP-PACKAGE usually spend more time in the fixup and
initialization steps than in relocation.)


@deffn {Function} RELOCATE-DUMPFILES (pathnames &optional base-address)

  Rewrite the dumpfiles so that they will, by default, load into
  non-overlapping parts of memory, starting with BASE-ADDRESS.
@end deffn

@deffn {Function} RELOCATE-DUMPFILE (pathname &optional base-address)

  Rewrite the dumpfile at PATHNAME so that it will load to BASE-ADDRESS
  by default.
@end deffn


@node Integration into REQUIRE and ASDF
@subsection Integration into REQUIRE and ASDF

sb-heapdump installs itself as a provider for REQUIRE.  Modules are
searched in each directory specified by SB-HEAPDUMP:*CENTRAL-REGISTRY*
with the downcased module name as file name and file type ".heap".

Heap files store in a registry directory should have been dumped using
the :SYSTEMS argument to DUMP-PACKAGE.

Dependencies of the systems as declared using :DEPENDS-ON are loaded
using REQUIRE.

Once a heap file has been found and loaded, it is automatically
registered as an ASDF system and ignored by the sb-heapfile's module
provider, so further invocations of REQUIRE and ASDF functions will
compile and load its components as usual.


@deffn {Variable} *CENTRAL-REGISTRY*

  A list of directory designators evaluated and searched in order when
  looking for heapfile modules.  Defaults to the current directory and
  $SBCL_HOME in this order.
@end deffn

@deffn {Generic Function} DUMP-SYSTEM (system)

  Convenience function that ASDF systems can define a method that will
  dump the system into a file.  See demo.lisp in the sb-heapdump
  distribution for examples.
@end deffn


@node Executable files
@subsection Executable files

@deffn {Function} MAKE-EXECUTABLE (heapfile &key output-pathname if-exists main-function)

  Create a file called OUTPUT-PATHNAME consisting of a trampoline binary
  and a copy of HEAPFILE.  (Optionally, an additional heapfile segment
  is appended that calls MAIN-FUNCTION with the binary's command line
  arguments in its initializer.)

  When executed, the generated file will run the `sbcl' binary as found
  in $PATH to load itself.

  OUTPUT-PATHNAME defaults to the name obtained by removing the type
  component from the pathname HEAPFILE.  For example, `foo.heap' is
  copied into `foo'.
@end deffn


@node DUMP-OBJECT behaviour for specific classes
@subsection DUMP-OBJECT behaviour for specific classes

The following types of objects can be dumped and are always dumped
literally:
@itemize
@item
    Immediate values (FIXNUM and CHARACTER)
@item
    BIGNUMs, SINGLE-FLOAT, DOUBLE-FLOAT, RATIO, COMPLEX
@item
    Lists
@item
    ARRAY (all types of arrays are supported, including single- and
    multi-dimensional arrays of all array element types known by SBCL, 
    whether simple or not.  This includes strings.)
@item
    Instances (technically, SB-KERNEL:INSTANCE), including structure
    instances, CLOS instances, and conditions.  [Note: CLOS support is
    experimental.]
@item
    Code components (if specified literally; see below for the fixup
    behaviour of functions)
@item
    Closures
@item
    Uninterned symbols
@item
    Value cells (fixme: whatever that is anyway)
@item
    System area pointers (SAPs)
@item
    Weak pointers.  (The weak pointer value will be dumped and the weak
    reference to it preserved if the value is either (i) reachable
    through a non-weak reference from the object graph being dumped or
    (ii) treated as a fixup.  Else the weak pointer will load as a
    broken reference.)
@end itemize

The following types of objects are dumped only if specified by the FORCE
argument, otherwise they are replaced by fixups.
@itemize
@item
    Packages
@item
    Interned symbols (forcing a package also forces all symbols with
    that package as their home package)
@item
    Classes (technically, all of SB-KERNEL:LAYOUT, SB-KERNEL:CLASSOID
    and SB-KERNEL:CLASS).  Forcing a symbol also forces classes named by
    that symbol.
    [FIXME! KLUDGE! There is an unnamed class in SBCL.  It is currently
    dumped unconditionally, which cannot be right.]
@item
    NAMED-TYPE: Named types are replaced by a fixup if named by a symbol
    that is not being forced.  The fixup will automatically re-create
    the named type at load time, if necessary.
@end itemize

The following types of objects are dumped according to more complex
heuristics.  (Notionally, these objects will be replaced by a fixup if
they are identified by a symbol that is not forced.)
@itemize
@item
    Except as noted below, ordinary functions (simple-funs) are replaced
    by fixups if all of the following conditions are true: (i) The
    function object itself is not being forced.  (ii) The function is
    named by a symbol or is named (SETF symbol).  (iii) The symbol is
    not being forced.  (iv) FDEFINITION for that function name actually
    returns the function object in question.  --- If a function is not
    replaced by a fixup, its code component is dumped, which implies
    dumping all its other entry points.
@item
    funcallable instances -- FIXME: except as noted below, funcallable
    instances are currently dumped unconditionally.  That can't be
    right, shouldn't the rules for simple-fun's apply here, too?
@item
    Generic functions: Slot accessors (SB-PCL::SLOT-ACCESSOR) are never
    dumped and instead recreated while loading the heap file, if
    necessary. Other generic function are treated like ordinary
    functions (see above).
@item
    An FDEFN object is replaced by a fixup unless any of the following
    conditions is true: (i) The function it points to is dumped
    literally.  (ii) Its name is a forced symbol.  (iii) Its name is a
    list containing a forced symbol.  (iv) It points to a CTOR or
    SLOT-ACCESSOR.
@end itemize

The following types of objects are never dumped literally:
@itemize
@item
    Although technically simple functions, SB-PCL::FAST-METHODs are
    never dumped literally and instead recreated while loading the heap
    file, if necessary.  [FIXME!  there are fast methods that are
    closures, what happens then?]
@item
    Although technically funcallable instances, SB-PCL::CTORs are never
    dumped and instead recreated while loading the heap file, if
    necessary.
@item
    ARRAY-TYPEs are never dumped and instead recreated while loading the
    heap file, if necessary.
@end itemize

fixme: are there CTYPE structures other than named-type and array-type
that can and need to be fixed up?  [union-type has a cache, but does the
compiler depend on that?]


@node DUMP-PACKAGES details
@subsection DUMP-PACKAGES details

For every package and every symbol that has one of these packages as its
home package, DUMP-PACKAGE installs an initializer that will restore
@itemize
@item
    all class cells named by this symbol
@item
    for all SPECIALIZER-DIRECT-METHODs of those classes, the
    method-function-plist of their FAST-METHODs
as well as most info-types in the compiler's INFO database for:
@item
    the symbol itself
@item
    the name (SETF symbol)
@item
    the slot reader, writer, and boundp accessors for all slot
    definitions of classes named by this symbol
@item
    the names of all the FAST-METHODs
@end itemize
